import { find, isEmpty, pickBy, values } from 'lodash'
import { ProseEditor } from 'substance'

import ContainerEditor from '../ContainerEditor'
import Comments from '../panes/Comments/CommentBoxList'
import CommentsProvider from '../panes/Comments/CommentsProvider'
import TrackChangesProvider from '../elements/track_change/TrackChangesProvider'
import SimpleExporter from '../SimpleEditorExporter'

class NotesEditor extends ProseEditor {
  didMount () {
    const provider = this.getProvider()
    provider.on('active:changed', this.scrollTo, this)

    this.editorSession.onUpdate('document', this.findNote, this)
    this.context.editorSession.notesEditorSession = this.editorSession
  }

  render ($$) {
    const el = $$('div').addClass('sc-notes-editor')

    const editor = this._renderEditor($$)

    // TODO -- why are we getting these from the component registry?
    const SplitPane = this.componentRegistry.get('split-pane')
    const ScrollPane = this.componentRegistry.get('scroll-pane')
    const Overlay = this.componentRegistry.get('overlay')

    const commentsPane = $$(Comments, {
      comments: this.props.comments,
      fragment: this.props.fragment,
      update: this.props.update,
      user: this.props.user
    }).addClass('sc-comments-pane')

    const editorWithComments = $$(SplitPane, {
      sizeA: '80%',
      splitType: 'vertical'
    }).append(
      editor,
      commentsPane
    )

    const contentPanel = $$(ScrollPane, {
      name: 'notesEditorContentPanel',
      scrollbarPosition: 'right'
    })
      .append(editorWithComments, $$(Overlay))
      .attr('id', 'content-panel-' + this.props.containerId)
      .ref('notesEditorContentPanel')

    el.append($$(SplitPane, { sizeA: '100%', splitType: 'horizontal' })
      .append(
        contentPanel,
        $$('div')   // TODO -- why are we appending an empty div at the end?
      ))

    return el
  }

  _renderEditor ($$) {
    const hasIsolatedNotes = this.getIsolatedNodes()
    const disabled = (isEmpty(hasIsolatedNotes))

    return $$(ContainerEditor, {
      book: this.props.book,
      comments: this.props.comments,
      containerId: this.props.containerId,
      configurator: this.props.configurator,
      editorSession: this.editorSession,
      disabled,
      history: this.props.history,
      fragment: this.props.fragment,
      spellcheck: 'native',
      trackChanges: this.props.trackChanges,
      trackChangesView: this.props.trackChangesView,
      user: this.props.user
    }).ref('notes_body')
    // TODO -- we need to be consistent in using camelCase in refs
  }

  scrollTo (calloutId) {
    const nodes = this.getIsolatedNodes()

    // TODO -- there is a findNote function further down
    const isolatednote = find(nodes, (c) => {
      return c.calloutId === calloutId
    })

    // TODO -- review
    if (isolatednote) {
      this.refs.notesEditorContentPanel.scrollTo(isolatednote.id)
      setTimeout(() => { this.setCursorFocus(isolatednote) })
    }
  }

  saveNote (isolatedNote) {
    const exporter = new SimpleExporter(this.props.configurator.config)
    const convertedNode = exporter.convertNode(isolatedNote)

    this.context.editorSession.transaction((tx, args) => {
      const path = [isolatedNote.calloutId, 'note-content']
      tx.set(path, convertedNode.innerHTML.trim())
    })
  }

  findNote () {
    const selection = this.editorSession.getSelection()
    if (!selection.end) return

    const isolatedNoteId = selection.end.path[0]
    const isolatedNote = this.editorSession.document.get(isolatedNoteId)
    this.context.editorSession.setSelection(null)
    this.saveNote(isolatedNote)
  }

  setCursorFocus (isolatedNote) {
    let newSel

    this.context.editorSession.setSelection(null)
    const selection = this.editorSession.getSelection()

    const length = isolatedNote.content.length
    const start = (selection.isNull()) ? length : selection.start.offset
    const end = (selection.isNull()) ? length : selection.end.offset
    this.editorSession.transaction((tx) => {
      newSel = tx.createSelection({
        type: 'property',
        surfaceId: `${this.props.containerId}/${isolatedNote.id}/${isolatedNote.id}.content`,
        containerId: 'notes', // wtf
        path: [isolatedNote.id, 'content'],
        startOffset: start,
        endOffset: end
      })
    })
    this.editorSession.setSelection(newSel)
  }

  getIsolatedNodes () {
    const doc = this.editorSession.document
    const nodes = doc.getNodes()

    const entries = pickBy(nodes, (value, key) => {
      return value.type === 'isolated-note'
    })

    return values(entries)
  }

  getProvider () {
    return this.context.notesProvider
  }

  getInitialState () {
    return {
      trackChangesView: this.props.trackChangesView
    }
  }

  getChildContext () {
    const oldContext = super.getChildContext()
    const doc = this.doc

    // comments provider
    const commentsProvider = new CommentsProvider(doc, {
      commandManager: this.commandManager,
      comments: this.props.fragment.comments,
      containerId: this.props.containerId,
      controller: this,
      editorSession: this.editorSession,
      fragment: this.props.fragment,
      surfaceManager: this.surfaceManager,
      update: this.props.update
    })

    const trackChangesProvider = new TrackChangesProvider(doc, {
      commandManager: this.commandManager,
      containerId: this.props.containerId,
      controller: this,
      editorSession: this.editorSession,
      surfaceManager: this.surfaceManager,
      user: this.props.user
    })

    // attach all to context
    return {
      ...oldContext,
      commentsProvider,
      trackChangesProvider
    }
  }
}

export default NotesEditor
