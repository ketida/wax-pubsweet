import { includes, some, isEmpty } from 'lodash'

import {
  ProseEditor,
  Toolbar
} from 'substance'

import Comments from './panes/Comments/CommentBoxList'
import CommentsProvider from './panes/Comments/CommentsProvider'
import ContainerEditor from './ContainerEditor'

import DiacriticsModal from './elements/diacritics/DiacriticsModal'

import Notes from './panes/Notes/Notes'
import NotesProvider from './panes/Notes/NotesProvider'

import TrackChangesProvider from './elements/track_change/TrackChangesProvider'

import ModalWarning from './elements/modal_warning/ModalWarning'

class Editor extends ProseEditor {
  constructor (parent, props) {
    super(parent, props)

    this.handleActions({
      trackChangesUpdate: this.updateTrackChange,
      trackChangesViewToggle: this.trackChangesViewToggle,
      changesNotSaved: this.changesNotSaved,
      closeModal: this.closeModal,
      scrollTo: this.scrollTo
    })

    this.on('diacritics:modal', this.toggleDiacritics, this)
  }

  trackChangesViewToggle () {
    this.extendState({
      trackChangesView: !this.state.trackChangesView
    })
  }

  updateTrackChange () {
    // TODO -- clean up this.props and this.refs
    this.extendProps({ trackChanges: !this.props.trackChanges })
    this.props.updateTrackChangesStatus(!this.props.trackChanges)
    this.refs.toolbar.extendProps({ trackChanges: this.props.trackChanges })
  }

  didMount () {
    this.extendState({ editorReady: true })
  }

  // TODO - break this all up into smaller pieces
  render ($$) {
    const el = $$('div').addClass('sc-prose-editor')

    // left side: editor and toolbar
    const toolbar = this._renderToolbar($$)
    const toolbarLeft = this._renderToolbarLeft($$)
    const editor = this._renderEditor($$)

    const SplitPane = this.componentRegistry.get('split-pane')
    const ScrollPane = this.componentRegistry.get('scroll-pane')
    const Overlay = this.componentRegistry.get('overlay')

    // TODO -- unnecessary // posssibly breaks book builder dnd
    const ContextMenu = this.componentRegistry.get('context-menu') // new what does it do?
    // let Dropzones = this.componentRegistry.get('dropzones') // new what does it do?

    const footerNotes = $$(Notes, {
      book: this.props.book,
      comments: this.props.fragment.comments,
      containerId: 'notes',
      history: this.props.history,
      disabled: this.props.disabled,
      fragment: this.props.fragment,
      trackChanges: this.props.trackChanges,
      trackChangesView: this.state.trackChangesView,
      update: this.props.update,
      user: this.props.user
    }).ref('footer-notes')

    const commentsPane = this.state.editorReady
    ? $$(Comments, {
      // TODO -- should probably remove the || {}
      comments: this.props.fragment.comments || {},
      fragment: this.props.fragment,
      update: this.props.update,
      user: this.props.user
    }).addClass('sc-comments-pane')
    : $$('div')

    const editorWithComments = $$(SplitPane, { sizeA: '80%', splitType: 'vertical' })
      .append(
        editor,
        commentsPane
      )

    const sideNav = $$('div')
      .addClass('sidenav')
      .addClass('sidenav-disabled')
      .append(toolbarLeft)

    const contentPanel = $$(ScrollPane, {
      name: 'contentPanel',
      scrollbarPosition: 'right'
    })
    .append(editorWithComments,
      $$(Overlay),
      $$(ContextMenu)
    )
    .attr('id', `content-panel-${this.props.containerId}`)
    .ref('contentPanel')

    const editorWithSidenav = $$(SplitPane, { sizeA: '20%', splitType: 'vertical' })
      .append(
        sideNav,
        contentPanel
      )

    const contentPanelWithSplitPane = $$(SplitPane, { sizeA: '95%', splitType: 'vertical' })
      .append(
        editorWithSidenav,
        $$('div')
      )

    const ToolbarWithEditor = $$(SplitPane, { splitType: 'horizontal' })
      .append(
        toolbar,
        contentPanelWithSplitPane
      )

    el.append(
      $$(SplitPane, { sizeA: '85%', splitType: 'horizontal' })
        .append(
          ToolbarWithEditor,
          footerNotes
        )
    )

    const modal = $$(ModalWarning, {
      width: 'medium',
      textAlign: 'center'
    })

    const diacriticsModal = $$(DiacriticsModal, {
      width: 'medium',
      textAlign: 'center'
    })

    if (this.state.diacriticsModal) {
      return el.append(diacriticsModal)
    }

    if (this.state.changesNotSaved) {
      return el.append(modal)
    }

    return el
  }

  // TODO -- leverage ProseEditor's this._renderToolbar maybe?
  _renderToolbar ($$) {
    const viewMode = this.props.disabled ? $$('span')
        .addClass('view-mode')
        .append('Editor is in Read-Only mode')
        : ''

    const commandStates = this.commandManager.getCommandStates()

    return $$('div')
      .addClass('se-toolbar-wrapper')
      .append(
        $$(Toolbar, {
          commandStates,
          trackChanges: this.props.trackChanges,
          trackChangesView: this.state.trackChangesView,
          toolGroups: [
            'document',
            'annotations',
            'diacritics-tool',
            'track-change-enable',
            'track-change-toggle-view'
          ]
        }).ref('toolbar')
      )
      .append(viewMode)
  }

  _renderToolbarLeft ($$) {
    const commandStates = this.commandManager.getCommandStates()

    return $$('div')
      .addClass('se-toolbar-wrapper')
      .append(
        $$(Toolbar, {
          commandStates,
          trackChanges: this.props.trackChanges,
          trackChangesView: this.state.trackChangesView,
          toolGroups: ['text', 'default']
        }
      )
      .ref('toolbarLeft')
    )
  }

  _renderEditor ($$) {
    const configurator = this.props.configurator
    const editing = this.props.disabled ? 'selection' : 'full'
    const disabled = (isEmpty(this.props.fragment.source) && editing === 'selection')

    return $$(ContainerEditor, {
      editing,
      disabled,
      editorSession: this.editorSession,
      commands: configurator.getSurfaceCommandNames(),
      containerId: 'body',
      spellcheck: 'native',
      textTypes: configurator.getTextTypes(),
      trackChanges: this.props.trackChanges,
      updateTrackChangesStatus: this.props.updateTrackChangesStatus,
      history: this.props.history,
      book: this.props.book
    }).ref('body')
  }

  getInitialState () {
    return {
      changesNotSaved: false,
      editorReady: false,
      trackChangesView: true,
      diacriticsModal: false
    }
  }

  canToggleTrackChanges () {
    const { user } = this.props
    const accepted = ['admin', 'production-editor', 'copy-editor']
    return some(accepted, role => includes(user.roles, role))
  }

  scrollTo (nodeId) {
    this.refs.contentPanel.scrollTo(nodeId)
  }

  // Modal funcionality

  changesNotSaved () {
    this.extendState({ changesNotSaved: true })
  }

  toggleDiacritics () {
    this.extendState({ diacriticsModal: true })
  }

  closeModal () {
    this.extendState({ changesNotSaved: false })
    this.extendState({ diacriticsModal: false })
  }

  getChildContext () {
    const oldContext = super.getChildContext()
    const doc = this.doc

    // toc provider
    // const tocProvider = new TOCProvider(doc, {
    //   containerId: this.props.containerId
    // })

    // notes provider
    const notesProvider = new NotesProvider(doc, {
      containerId: this.props.containerId
    })

    // comments provider
    const commentsProvider = new CommentsProvider(doc, {
      commandManager: this.commandManager,
      comments: this.props.fragment.comments,
      containerId: this.props.containerId,
      controller: this,
      editorSession: this.editorSession,
      fragment: this.props.fragment,
      surfaceManager: this.surfaceManager,
      update: this.props.update
    })

    // TODO -- do I need all of these?
    // track changes provider
    const trackChangesProvider = new TrackChangesProvider(doc, {
      commandManager: this.commandManager,
      containerId: this.props.containerId,
      controller: this,
      editorSession: this.editorSession,
      surfaceManager: this.surfaceManager,
      user: this.props.user
    })

    // attach all to context
    return {
      ...oldContext,
      notesProvider,
      commentsProvider,
      trackChangesProvider
    }
  }

  dispose () {
    this.editorSession.dragManager.dispose()
  }
}

export default Editor
