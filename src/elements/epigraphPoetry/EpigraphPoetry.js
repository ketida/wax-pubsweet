import { TextBlock } from 'substance'

class EpigraphPoetry extends TextBlock {}

EpigraphPoetry.type = 'epigraph-poetry'

export default EpigraphPoetry
