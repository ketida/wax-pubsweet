import EpigraphPoetry from './EpigraphPoetry'
import EpigraphPoetryComponent from './EpigraphPoetryComponent'
import EpigraphPoetryHTMLConverter from './EpigraphPoetryHTMLConverter'

export default {
  name: 'epigraph-poetry',
  configure: (config) => {
    config.addNode(EpigraphPoetry)

    config.addComponent(EpigraphPoetry.type, EpigraphPoetryComponent)
    config.addConverter('html', EpigraphPoetryHTMLConverter)

    config.addTextType({
      name: 'epigraph-poetry',
      data: { type: 'epigraph-poetry' }
    })

    config.addLabel('epigraph-poetry', {
      en: 'Epigraph: Poetry'
    })
  }
}
