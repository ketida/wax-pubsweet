import { TextBlockComponent } from 'substance'

class EpigraphPoetryComponent extends TextBlockComponent {
  render ($$) {
    const el = super.render($$)
    return el.addClass('sc-blockquote')
  }
}

export default EpigraphPoetryComponent
