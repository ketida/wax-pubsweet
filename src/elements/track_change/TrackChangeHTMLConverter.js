export default {
  type: 'track-change',
  tagName: 'track-change',

  import: function (element, node, converter) {
    node.status = element.attr('status')
    node.user = {
      id: element.attr('user-id'),
      roles: element.attr('roles').split(','),
      username: element.attr('username')
    }
  },

  export: function (node, element, converter) {
    const { status, user } = node

    element.setAttribute('status', status)

    element.setAttribute('user-id', user.id)
    element.setAttribute('roles', user.roles.join(','))
    element.setAttribute('username', user.username)
  }
}
