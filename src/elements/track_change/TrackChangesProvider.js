import {
  clone,
  findIndex,
  includes,
  map,
  pickBy,
  some,
  sortBy
} from 'lodash'

import { TOCProvider } from 'substance'

import {
  getAnnotationByStatus,
  isAnnotationFromTheSameUser,
  isNotOnTrackAnnotation,
  isOnAnnotation,
  isSelectionOnLeftEdge,
  isSelectionOnRightEdge
} from './utils/annotationHelpers'

import {
  createAdditionAnnotationOnLastChar,
  deleteAllOwnAdditions,
  deleteOrMergeAllOwnDeletions,
  deleteSelectedAndCreateAddition,
  expandAnnotationToDirection,
  insertCharacterWithAddAnnotation,
  insertCharacterWithoutExpandingAnnotation,
  markSelectionAsDeleted,
  selectCharacterAndMarkDeleted
} from './utils/handlerHelpers'

// import { handleUndoRedo } from './utils/historyHandlers'

import {
  isSelectionCollapsed,
  isSelectionContainedWithin,
  moveCursorTo,
  updateSelection
} from './utils/selectionHelpers'

import {
  deleteCharacter,
  deleteSelection,
  insertText,
  removeTrackAnnotation
} from './utils/transformations'

class TrackChangesProvider extends TOCProvider {
  constructor (document, config) {
    super(document, config)
    // config.documentSession.on('didUpdate', handleUndoRedo, this)

    // handle button actions
    const editor = this.config.controller

    editor.handleActions({
      'trackChangesViewUpdate': () => {
        editor.extendState({ trackChangesView: !editor.state.trackChangesView })
        editor.emit('ui:updated')
      }
    })
  }

  /*

    HANDLERS

  */

  handleTransaction (options) {
    // options.editor = this.config.controller
    options.editorSession = this.config.editorSession
    options.selection = this.getSelection()
    options.surface = this.getSurface()
    options.user = this.config.user

    this.chooseHanlder(options)
  }

  chooseHanlder (options) {
    if (options.status === 'add') return this.handleAdd(options)
    if (options.status === 'delete') return this.handleDelete(options)
  }

  handleAdd (options) {
    const isCollapsed = isSelectionCollapsed(options)

    if (isCollapsed) return this.handleAddCollapsed(options)
    if (!isCollapsed) return this.handleAddNonCollapsed(options)
  }

  handleAddCollapsed (options) {
    const notOnTrack = isNotOnTrackAnnotation(options)
    const isOnAdd = isOnAnnotation(options, 'add')
    const isOnDelete = isOnAnnotation(options, 'delete')

    if (notOnTrack) return insertCharacterWithAddAnnotation(options)

    if (isOnAdd) {
      // annotation gets expanded automatically, unless the selection is on its left edge
      const annotation = getAnnotationByStatus(options, 'add')
      const isOnLeftEdge = isSelectionOnLeftEdge(options, annotation)
      const isOnRightEdge = isSelectionOnRightEdge(options, annotation)
      const isFromSameUser = isAnnotationFromTheSameUser(options, annotation)
      const mode = this.getMode()

      if (isFromSameUser) {
        insertText(options)
        if (isOnLeftEdge) expandAnnotationToDirection(options, annotation)
      }

      if (!isFromSameUser) {
        if (isOnRightEdge) {
          insertCharacterWithoutExpandingAnnotation(options, annotation)
        } else {
          insertText(options)
        }

        if (mode) createAdditionAnnotationOnLastChar(options)
      }

      return
    }

    if (isOnDelete) {
      const annotation = getAnnotationByStatus(options, 'delete')

      const isOnLeftEdge = isSelectionOnLeftEdge(options, annotation)
      const isOnRightEdge = isSelectionOnRightEdge(options, annotation)
      const withinAnnotation = isSelectionContainedWithin(options, annotation, true)

      // if contained within the delete annotation, move it to the edge,
      // insert character, set event to null so that the character does
      // not get inserted twice, and handle again
      if (withinAnnotation) {
        moveCursorTo(options, annotation.end.offset)
        insertCharacterWithoutExpandingAnnotation(options, annotation)
        options.selection = this.getSelection()
        return this.handleAdd(options)
      }

      if (isOnLeftEdge) return insertCharacterWithAddAnnotation(options)

      if (isOnRightEdge) {
        insertCharacterWithoutExpandingAnnotation(options, annotation)
        return this.handleAdd(options)
      }
    }
  }

  handleAddNonCollapsed (options) {
    let { selection } = options

    const notOnTrack = isNotOnTrackAnnotation(options)
    const isOnDelete = isOnAnnotation(options, 'delete')

    if (notOnTrack) return deleteSelectedAndCreateAddition(options)

    // delete all additions of the same user and
    // shorten selection by the number of deleted characters
    const shortenBy = deleteAllOwnAdditions(options, selection)
    const startOffset = selection.start.offset
    const endOffset = selection.end.offset - shortenBy
    selection = updateSelection(options, selection, startOffset, endOffset)
    options.selection = selection

    if (isOnDelete) {
      const annotation = getAnnotationByStatus(options, 'delete')
      const withinAnnotation = isSelectionContainedWithin(options, annotation)

      if (withinAnnotation) {
        // if selection is wholly contained within a delete annotation,
        // move to the end of the annotation and handle again
        moveCursorTo(options, annotation.end.offset)
        options.selection = this.getSelection()
        return this.handleAddCollapsed(options)
      }
    }

    // after deleting all own additions, there is still text selected
    // mark it as deleted and add new addition annotation at the end
    // TODO -- use selection.isCollapsed()
    if (selection.end.offset > selection.start.offset) {
      deleteOrMergeAllOwnDeletions(options, selection)
      deleteSelectedAndCreateAddition(options)
      return
    }

    // if you got here, deleting all own additions left a collapsed selection
    // (ie. the whole selection was on an own addition annotation)
    // since selection is collapsed, handle it as such
    return this.handleAddCollapsed(options)
  }

  handleDelete (options) {
    const { key, move } = options
    const isCollapsed = isSelectionCollapsed(options)

    options.direction = {
      cursorTo: (move === 'left') ? 'start' : 'end',
      key: key,
      move: move
    }

    if (isCollapsed) return this.handleDeleteCollapsed(options)
    if (!isCollapsed) return this.handleDeleteNonCollapsed(options)
  }

  handleDeleteCollapsed (options) {
    const { direction } = options

    const notOnTrack = isNotOnTrackAnnotation(options)
    const isOnAdd = isOnAnnotation(options, 'add')
    const isOnDelete = isOnAnnotation(options, 'delete')

    if (notOnTrack) return selectCharacterAndMarkDeleted(options)

    if (isOnAdd) {
      const annotation = getAnnotationByStatus(options, 'add')
      const isOnLeftEdge = isSelectionOnLeftEdge(options, annotation)
      const isOnRightEdge = isSelectionOnRightEdge(options, annotation)
      const isFromSameUser = isAnnotationFromTheSameUser(options, annotation)
      const mode = this.getMode()
      const key = direction.key

      // use this variable to throw the flow to the isOnDelete handler underneath
      let pass = false

      // when on own additions, simply delete the character
      // unless it is on the edge: then make a deletion annotation

      // TODO -- watch it for different users
      if (
        (isOnLeftEdge && key === 'BACKSPACE') ||
        (isOnRightEdge && key === 'DELETE') ||
        (!isFromSameUser && !isOnDelete)
      ) {
        if (mode) return selectCharacterAndMarkDeleted(options)
        pass = true
      }

      if (!isFromSameUser && isOnDelete) pass = true
      if (!pass) return deleteCharacter(options)
    }

    if (isOnDelete) {
      const annotation = getAnnotationByStatus(options, 'delete')
      const isOnLeftEdge = isSelectionOnLeftEdge(options, annotation)
      const isOnRightEdge = isSelectionOnRightEdge(options, annotation)
      const isFromSameUser = isAnnotationFromTheSameUser(options, annotation)
      const key = direction.key

      let moveOnly, point

      if (!isOnLeftEdge && !isOnRightEdge) {
        point = annotation[direction.cursorTo + 'Offset']
        moveOnly = true
      } else if (isOnLeftEdge && key === 'DELETE') {
        point = annotation.end.offset
        moveOnly = true
      } else if (isOnRightEdge && key === 'BACKSPACE') {
        point = annotation.start.offset
        moveOnly = true
      }

      if (moveOnly) {
        return moveCursorTo(options, point)
      }

      if (isFromSameUser) {
        options.cursorTo = direction.cursorTo
        return expandAnnotationToDirection(options, annotation)
      } else {
        return selectCharacterAndMarkDeleted(options)
      }
    }
  }

  handleDeleteNonCollapsed (options) {
    const { direction } = options
    let { selection } = options

    const notOnTrack = isNotOnTrackAnnotation(options)
    const isOnDelete = isOnAnnotation(options, 'delete')

    if (notOnTrack) return markSelectionAsDeleted(options)

    const shortenBy = deleteAllOwnAdditions(options, selection)
    const startOffset = selection.start.offset
    const endOffset = selection.end.offset - shortenBy

    updateSelection(options, selection, startOffset, endOffset)

    // TODO -- validate that this is not needed
    // if (selection.isCollapsed()) return this.handleDeleteCollapsed(options)
    if (selection.isCollapsed()) return

    if (isOnDelete) {
      const annotation = getAnnotationByStatus(options, 'delete')
      const containedWithin = isSelectionContainedWithin(options, annotation)

      if (containedWithin) {
        const point = annotation[direction.cursorTo + 'Offset']
        return moveCursorTo(options, point)
      }
    }

    options.selection = deleteOrMergeAllOwnDeletions(options, selection)
    // options.selection = this.getSelection()
    markSelectionAsDeleted(options)
  }

  /*

    ACCEPT / REJECT

  */

  resolve (annotation, action) {
    // const next = this.getNext(annotation)
    // const selection = annotation.getSelection()
    const status = annotation.status

    let options = {
      annotation: annotation,
      surface: this.getSurface()
    }

    removeTrackAnnotation(options)

    if (
      (action === 'accept' && status === 'delete') ||
      (action === 'reject' && status === 'add')
    ) {
      options.selection = annotation.getSelection()
      deleteSelection(options)
    }

    // this.focus(next)
  }

  computeEntries () {
    const doc = this.getDocument()
    const nodes = doc.getNodes()

    const changes = pickBy(nodes, node => {
      return node.type === 'track-change'
    })

    const entries = this.sortNodes(changes)
    return entries
  }

  reComputeEntries () {
    this.entries = this.computeEntries()
  }

  sortNodes (nodes) {
    const changes = clone(nodes)
    const containerId = this.config.containerId
    const doc = this.getDocument()
    const container = doc.get(containerId)

    const changesArray = map(changes, annotation => {
      const blockId = annotation.path[0]
      const blockPosition = container.getPosition(blockId)
      const nodePosition = annotation.start.offset

      return {
        id: annotation.id,
        blockPosition: blockPosition,
        nodePosition: nodePosition,
        node: annotation
      }
    })

    return sortBy(changesArray, ['blockPosition', 'nodePosition'])
  }

  getNext (annotation) {
    const entries = this.entries
    if (entries.length <= 1) return

    const position = findIndex(entries, change => {
      return change.node.id === annotation.id
    })
    if (position === -1) return
    const next = position + 1

    if (next >= entries.length) return entries[0].node
    return entries[next].node
  }

  focus (annotation) {
    if (!annotation) return

    const surface = this.getSurface()
    const { controller } = this.config

    controller.scrollTo(annotation.id)

    const selection = annotation.getSelection()

    surface.editorSession.setSelection(selection)
    moveCursorTo({ surface }, 'start')
  }

  canAct () {
    const { user } = this.config
    const accepted = ['admin', 'production-editor', 'copy-editor']
    return some(accepted, (role) => includes(user.roles, role))
  }

  /*

    GETTERS

  */

  getMode () {
    const commandStates = this.config.commandManager.getCommandStates()
    const trackState = commandStates['track-change']
    return trackState.mode
  }

  getSelection () {
    const surface = this.getSurface()
    return surface.domSelection.getSelection()
  }

  getSurface () {
    const surfaceManager = this.config.surfaceManager
    const id = this.config.containerId

    return surfaceManager.getSurface(id)
  }
}

TrackChangesProvider.tocTypes = ['track-change']

export default TrackChangesProvider
