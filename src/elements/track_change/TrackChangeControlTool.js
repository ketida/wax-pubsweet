import { Tool } from 'substance'

class TrackChangeControlTool extends Tool {
  constructor (parent, props, config) {
    super(parent, props, config)
    this.disabled = false
  }

  getClassNames () {
    return 'sm-target-track-change-enable'
  }

  willReceiveProps (props) {
    if (!this.canAct()) this.disabled = true
  }

  renderButton ($$) {
    const el = super.renderButton($$)

    const trackChangesMode = this.isTrackChangesOn()
    if (trackChangesMode === true) el.addClass('track-changes-active')

    if (!this.canAct()) el.attr('disabled', 'true')
    return el
  }

  isTrackChangesOn () {
    // TODO -- ??
    return this.parent._owner.props.trackChanges
  }

  canAct () {
    const provider = this.context.trackChangesProvider
    return provider.canAct()
  }
}

TrackChangeControlTool.type = 'track-change-enable'

export default TrackChangeControlTool
