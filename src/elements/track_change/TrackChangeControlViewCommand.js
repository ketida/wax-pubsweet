import { Command } from 'substance'

class TrackChangeControlViewCommand extends Command {
  getCommandState (params) {
    const newState = {
      active: false,
      disabled: false
    }

    return newState
  }

  // TODO -- review
  execute (params, context) {
    const surface = context.surfaceManager.getSurface('body')

    surface.send('trackChangesViewToggle')
    surface.rerender()

    // put the cursor back where it was on the surface
    if (!params.selection.isNull()) {
      params.editorSession.setSelection(params.selection)
    }

    return true
  }
}

TrackChangeControlViewCommand.type = 'track-change-toggle-view'

export default TrackChangeControlViewCommand
