const getSelection = (surface) => {
  return surface.domSelection.getSelection()
}

const isSelectionCollapsed = (options) => {
  // const selection = this.getSelection()
  const { selection } = options
  const isCollapsed = selection.isCollapsed()
  return isCollapsed
}

// TODO -- refactor this and isAnnotationContainedWithinSelection into one
const isSelectionContainedWithin = (options, annotation, strict) => {
  // const selection = this.getSelection()
  const selection = getSelection(options.surface)

  const annotationSelection = annotation.getSelection()

  return annotationSelection.contains(selection, strict)

  // const leftSide = (selection.startOffset < annotation.startOffset)
  // const rightSide = (selection.endOffset > annotation.endOffset)
  //
  // if (leftSide || rightSide) return false
  // return true
}

const moveCursorTo = (options, point) => {
  // const selection = sel || this.getSelection()
  // const { selection, surface } = options
  const { surface } = options
  const selection = getSelection(surface)
  // const surface = this.getSurface()

  // TODO -- use substance's selection.collapse(direction)
  if (point === 'start') {
    selection.end.offset = selection.start.offset
  } else if (point === 'end') {
    selection.start.offset = selection.end.offset
  } else {
    selection.start.offset = point
    selection.end.offset = point
  }

  surface.editorSession.setSelection(selection)
}

// const setSelectionPlusOne = (direction) {
const setSelectionPlusOne = (options, direction) => {
  const { selection, surface } = options

  if (direction === 'left') selection.start.offset -= 1
  if (direction === 'right') selection.end.offset += 1

  surface.editorSession.setSelection(selection)

  return selection
}

const updateSelection = (options, selection, startOffset, endOffset) => {
  const { surface } = options

  selection.start.offset = startOffset
  selection.end.offset = endOffset

  surface.editorSession.setSelection(selection)
  return selection
}

export {
  getSelection,
  isSelectionCollapsed,
  isSelectionContainedWithin,
  moveCursorTo,
  setSelectionPlusOne,
  updateSelection
}
