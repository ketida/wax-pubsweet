import { TextPropertyEditor as SubstanceTextPropertyEditor } from 'substance'

/*
  TODO -- SERIOUS REVIEW, we shouldn't need to duplicate code here.
  This has duplicate overrides of all track changes functions from our
  container editor. This is because TextProperyEditor extends Surface directly.
*/

class TextPropertyEditor extends SubstanceTextPropertyEditor {
  onTextInput (event) {
    if (!this.isTrackChangesOn()) {
      return super.onTextInput(event)
    }

    let surface = this.getSurface()
    if (!surface) return
    return surface.onTextInput(event)
  }

  onTextInputShim (event) {
    if (!this.isTrackChangesOn()) {
      return super.onTextInputShim(event)
    }

    let surface = this.getSurface()
    if (!surface) return
    return surface.onTextInputShim(event)
  }

  _handleDeleteKey (event) {
    // If isloated note has no content and you keep pressing backspace,
    // it gets deleted.
    const selection = this.editorSession.getSelection()
    if (selection.start.offset === 0 && selection.end.offset === 0) {
      super.onTextInputShim(event)
      return super.onTextInput(event)
    }

    if (!this.isTrackChangesOn()) {
      return super._handleDeleteKey(event)
    }

    let surface = this.getSurface()
    if (!surface) return
    return surface._handleDeleteKey(event)
  }

  _handleSpaceKey (event) {
    if (!this.isTrackChangesOn()) {
      return super._handleSpaceKey(event)
    }

    let surface = this.getSurface()
    if (!surface) return
    return surface._handleSpaceKey(event)
  }

  isTrackChangesOn () {
    return this.context.editor.props.trackChanges
  }

  getSurface () {
    const containerId = this.getContainerId()
    return this.context.surfaceManager.getSurface(containerId)
  }

  getContainerId () {
    return this.context.editor.props.containerId
  }
}

export default TextPropertyEditor
