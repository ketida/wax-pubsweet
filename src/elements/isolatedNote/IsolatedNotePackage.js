import IsolatedNote from './IsolatedNote'
import IsolatedNoteComponent from './IsolatedNoteComponent'
import IsolatedNoteHTMLConverter from './IsolatedNoteHTMLConverter'

export default {
  name: 'isolated-note',
  configure: function (config) {
    config.addNode(IsolatedNote)
    config.addComponent(IsolatedNote.type, IsolatedNoteComponent)
    config.addConverter('html', IsolatedNoteHTMLConverter)
  }
}
