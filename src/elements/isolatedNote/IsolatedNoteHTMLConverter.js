// TODO
// review tag name
// this converter shouldn't exist

export default {
  type: 'isolated-note',
  tagName: 'isolated-note',

  import: function (el, node, converter) {
    node.content = converter.annotatedText(el, [node.id, 'content'])
    node.calloutId = el.attr('data-parent-id')
    node.position = el.attr('data-position')
  },

  export: function (node, el, converter) {
    el.append(
      converter.annotatedText([node.id, 'content'])
    )

    el.setAttribute('data-parent-id', node.calloutId)
  }
}
