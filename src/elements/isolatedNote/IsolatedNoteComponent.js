import { IsolatedNodeComponent } from 'substance'
import TextPropertyEditor from './TextPropertyEditor'

class IsolatedNoteComponent extends IsolatedNodeComponent {
  constructor (...props) {
    super(...props)
    const provider = this.getProvider()
    provider.on('remove:activeIsolatedNote', this.removeActive, this)
  }

  render ($$) {
    // TODO
    // why class sc-entity??
    // rename dataId to calloutId ?? if so, change in provider
    // is there a better way than rendering a text property editor???
    // review ref name
    // do we need a ref ??
    // why are we giving it a p tagname??

    const { node } = this.props
    const { mode } = this.state
    const provider = this.getProvider()

    const editing = this.isDisabled() ? 'selection' : 'full'

    const textPropertyEditor = $$(TextPropertyEditor, {
      editing,
      multiLine: false,
      path: [node.id, 'content'],
      tagName: 'p'
    })
    .ref(`noteContentEditor-${node.id}`)

    const position = $$('div')
      .append(`${node.position}.`)
      .addClass('position')
      .on('click', this.focusCallout)

    const el = $$('div')
      .append(position)
      .addClass('sc-entity')
      .addClass('sc-isolated-node')
      .attr('data-id', node.id)
      .append(textPropertyEditor)

    if (mode === 'focused') {
      el.addClass('note-active')
      provider.setActiveNote(node.calloutId)
    }

    return el
  }

  removeActive () {
    this.context.editorSession.setSelection(null)
  }

  focusCallout () {
    const provider = this.getProvider()
    provider.emit('active:changed', this.props.node.calloutId)
    this.send('scrollTo', this.props.node.calloutId)
  }

  isDisabled () {
    const { editor } = this.context
    return editor.props.disabled
  }

  getProvider () {
    return this.context.notesProvider
  }
}

export default IsolatedNoteComponent
