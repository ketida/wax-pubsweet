import { TextBlockComponent } from 'substance'

class ChapterSubtitleComponent extends TextBlockComponent {
  render ($$) {
    const el = super.render($$)
    return el.addClass('sc-chapter-subtitle')
  }
}

export default ChapterSubtitleComponent
