import { TextBlockComponent, TextPropertyComponent } from 'substance'

class SourceNoteComponent extends TextBlockComponent {
  render ($$) {
    // Using NodeComponent's and TextBlockComponent's render functions so that
    // we can pass an extra prop (withoutBreak) to TextProperty

    const { node } = this.props
    const tagName = this.getTagName()

    const el = $$(tagName)
      .attr('data-id', node.id)
      .addClass(this.getClassNames())

    el.addClass('sc-text-block')

    if (node.direction) {
      el.attr('dir', node.direction)
    }

    if (node.textAlign) {
      el.addClass(`sm-align-${node.textAlign}`)
    }

    el.append($$(TextPropertyComponent, {
      direction: node.direction,
      path: node.getTextPath(),
      withoutBreak: true
    }))

    el.addClass('sc-source-note')

    return el
  }
}

export default SourceNoteComponent
