import SourceNote from './SourceNote'
// import SourceNoteCommand from './SourceNoteCommand'
import SourceNoteComponent from './SourceNoteComponent'
import SourceNoteHTMLConverter from './SourceNoteHTMLConverter'
// import SourceNoteTool from './SourceNoteTool'

export default {
  name: 'source-note',
  configure: (config) => {
    config.addNode(SourceNote)

    config.addComponent(SourceNote.type, SourceNoteComponent)
    config.addConverter('html', SourceNoteHTMLConverter)

    // config.addCommand(SourceNote.type, SourceNoteCommand, { nodeType: SourceNote.type })
    // config.addTool('source-note', SourceNoteTool, { toolGroup: 'annotations' })

    config.addTextType({
      name: 'source-note',
      data: { type: 'source-note' }
    })

    // config.addIcon('source-note', { fontawesome: 'fa-book' })
    config.addLabel('source-note', {
      en: 'Source Note'
    })
  }
}
