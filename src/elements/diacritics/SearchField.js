/* eslint react/prop-types: 0 */
import { Component } from 'substance'

class SearchField extends Component {
  render ($$) {
    const searchInput = $$('div')
      .addClass('diacritics-search-container')
      .append($$('span')
      .addClass('search-text')
      .append('Search For Characters '))
      .append($$('input')
      .addClass('search-input')
      .attr('id', 'search-diacrtics')
      .attr('name', 'search')
      .attr('value', '')
      .attr('autocomplete', 'off')
      .on('keyup', this.props.onKeyUp)
      .ref('search-diacrtics')
  )
    return searchInput
  }
}

export default SearchField
