import { Command } from 'substance'

class DiacriticsCommand extends Command {
  getCommandState (params) {
    const sel = params.selection
    const surface = params.surface
    const newState = {
      active: false,
      disabled: true
    }

    // TODO -- clean up long chains
    if (params.editorSession.notesEditorSession) {
      const editorSession = params.editorSession
      const notesEditorSelection = params.editorSession.notesEditorSession.getSelection()
      if (!notesEditorSelection.isNull()) {
        editorSession.notesEditorSelection = notesEditorSelection
        newState.disabled = false
        return newState
      }
    }

    if (sel && !sel.isNull() && !sel.isCustomSelection() &&
        surface && surface.isContainerEditor()) {
      newState.disabled = false
    }
    return newState
  }

  execute (params) {
    // TODO -- body should not be here explicitly
    const surface = params.editorSession.surfaceManager.getSurface('body')
    surface.context.editor.emit('diacritics:modal')
  }
}

DiacriticsCommand.type = 'diacritics-tool'

export default DiacriticsCommand
