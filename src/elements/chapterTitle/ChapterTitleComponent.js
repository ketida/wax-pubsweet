import { TextBlockComponent } from 'substance'

class ChapterTitleComponent extends TextBlockComponent {
  render ($$) {
    const el = super.render($$)
    return el.addClass('sc-chapter-title')
  }
}

export default ChapterTitleComponent
