import EpigraphProse from './EpigraphProse'
import EpigraphProseComponent from './EpigraphProseComponent'
import EpigraphProseHTMLConverter from './EpigraphProseHTMLConverter'

export default {
  name: 'epigraph-prose',
  configure: (config) => {
    config.addNode(EpigraphProse)

    config.addComponent(EpigraphProse.type, EpigraphProseComponent)
    config.addConverter('html', EpigraphProseHTMLConverter)

    config.addTextType({
      name: 'epigraph-prose',
      data: { type: 'epigraph-prose' }
    })

    config.addLabel('epigraph-prose', {
      en: 'Epigraph: Prose'
    })
  }
}
