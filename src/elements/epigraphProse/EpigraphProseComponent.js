import { TextBlockComponent } from 'substance'

class EpigraphProseComponent extends TextBlockComponent {
  render ($$) {
    const el = super.render($$)
    return el.addClass('sc-blockquote')
  }
}

export default EpigraphProseComponent
