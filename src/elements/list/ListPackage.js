import ListNode from './ListNode'
import ListItemNode from './ListItemNode'
import ListComponent from './ListComponent'
import ListHTMLConverter from './ListHTMLConverter'
import ListItemHTMLConverter from './ListItemHTMLConverter'
import InsertListCommand from './InsertListCommand'
import InsertListTool from './InsertListTool'

export default {
  name: 'list',
  configure: (config, { toolGroup, disableCollapsedCursor }) => {
    config.addNode(ListNode)
    config.addNode(ListItemNode)
    config.addComponent('list', ListComponent)

    /*
      Ordered lists
    */

    config.addCommand('insert-ordered-list', InsertListCommand, {
      nodeType: 'list',
      ordered: true,
      disableCollapsedCursor
    })
    config.addTool('insert-ordered-list', InsertListTool, { toolGroup })
    config.addLabel('insert-ordered-list', {
      en: 'Ordered list'
    })
    config.addIcon('insert-ordered-list', { fontawesome: 'fa-list-ol' })

    /*
      Unstyled lists
    */

    config.addCommand('insert-unstyled-list', InsertListCommand, {
      nodeType: 'list',
      ordered: true,
      custom: 'unstyled',
      disableCollapsedCursor
    })
    config.addTool('insert-unstyled-list', InsertListTool, { toolGroup })
    config.addLabel('insert-unstyled-list', {
      en: 'Undecorated list'
    })
    config.addIcon('insert-unstyled-list', { fontawesome: 'fa-bars' })

    /*
      Unordered lists
    */

    config.addCommand('insert-unordered-list', InsertListCommand, {
      nodeType: 'list',
      ordered: false,
      disableCollapsedCursor
    })
    config.addTool('insert-unordered-list', InsertListTool, { toolGroup })
    config.addLabel('insert-unordered-list', {
      en: 'Unordered list'
    })
    config.addIcon('insert-unordered-list', { fontawesome: 'fa-list-ul' })

    /*
      Dialogue
    */

    config.addCommand('insert-qa-list', InsertListCommand, {
      nodeType: 'list',
      ordered: true,
      custom: 'qa',
      disableCollapsedCursor
    })
    config.addTool('insert-qa-list', InsertListTool, { toolGroup: 'text' })
    config.addLabel('insert-qa-list', {
      en: 'QA list'
    })
    config.addIcon('insert-qa-list', { fontawesome: 'fa-quora' })

    /*
      Converters
    */

    config.addConverter('html', ListHTMLConverter)
    config.addConverter('html', ListItemHTMLConverter)
  }
}
