import ChapterNumber from './ChapterNumber'
import ChapterNumberComponent from './ChapterNumberComponent'
import ChapterNumberHTMLConverter from './ChapterNumberHTMLConverter'

export default {
  name: 'extract',
  configure: (config) => {
    config.addNode(ChapterNumber)

    config.addComponent(ChapterNumber.type, ChapterNumberComponent)
    config.addConverter('html', ChapterNumberHTMLConverter)

    config.addTextType({
      name: 'chapter-number',
      data: { type: 'chapter-number' }
    })

    config.addLabel('chapter-number', {
      en: 'Chapter Number'
    })
  }
}
