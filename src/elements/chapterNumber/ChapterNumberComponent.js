import { TextBlockComponent } from 'substance'

class ExtractComponent extends TextBlockComponent {
  render ($$) {
    const el = super.render($$)
    return el.addClass('sc-chapter-number')
  }
}

export default ExtractComponent
