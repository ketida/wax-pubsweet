import { TextBlock } from 'substance'

class ChapterNumber extends TextBlock {}

ChapterNumber.type = 'chapter-number'

export default ChapterNumber
