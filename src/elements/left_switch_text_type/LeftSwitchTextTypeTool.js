import { filter } from 'lodash'
import { forEach, Tool } from 'substance'

// TODO -- why forEach from substance and not lodash?

class LeftSwitchTextTypeTool extends Tool {
  constructor (...args) {
    super(...args)

    const { editorSession } = this.context
    const { configurator } = editorSession

    this.extendProps({
      textTypes: configurator.config.textTypes
    })
  }

  // TODO -- necessary?
  didMount (...args) {
    super.didMount(...args)
  }

  // TODO -- rewrite all of this
  // TODO -- obviously break into components
  render ($$) {
    const { labelProvider } = this.context
    const { currentTextType, textTypes } = this.props

    const el = $$('ul')
      .addClass('sc-switch-text-type')
      .append(
        $$('li')
          .append('Display')
          .addClass('heading')
      )

    const headings = []

    const texts = filter(textTypes, (type) => {
      const generalType = type.spec.data.type
      const textGroup = (generalType === 'extract-poetry' ||
      generalType === 'paragraph' || generalType === 'extract-prose' ||
      generalType === 'source-note')

      if (textGroup) {
        headings.push(type)

        if (generalType !== 'source-note') {
          return false
        }
      }
      return true
    })

    forEach(texts, (type) => {
      const name = type.spec.name

      const item = $$('li')
          .addClass(`sm ${name}`)
          .addClass('se-option')
          .attr('data-type', name)
          .append(labelProvider.getLabel(name))
          .on('click', this.handleClick)
          .ref(`li-${name}`)

      if (currentTextType && name === currentTextType.name) {
        item.addClass('active')
      }

      el.append(item)
    })

    el.append(
      $$('li')
        .append('Text')
        .addClass('heading')
    )

    forEach(headings, (type) => {
      const name = type.spec.name

      const item = $$('li')
          .addClass(`sm ${name}`)
          .addClass('se-option')
          .attr('data-type', name)
          .append(labelProvider.getLabel(name))
          .on('click', this.handleClick)
          // .ref(`li-${name}`)

      if (currentTextType && name === currentTextType.name) {
        item.addClass('active')
      }

      el.append(item)
    })

    if ((currentTextType && currentTextType.name === 'container-selection') ||
        this.isReadOnlyMode()
       ) {
      el.addClass('sm-disabled')
    }

    return el
  }

  executeCommand (textType) {
    const { commandManager } = this.context
    const name = this.getCommandName()

    // TODO -- do we need to CALL the function in the argument??
    commandManager.executeCommand(name, { textType })
  }

  // TODO -- needs comments
  handleClick (event) {
    event.preventDefault()
    const switchTextType = this.context.commandManager.commandStates['switch-text-type']
    if (this.isReadOnlyMode() ||
    switchTextType.disabled === true) return

    const type = event.currentTarget.dataset.type
    this.executeCommand(type)
  }

  getContainerId () {
    const editor = this.getEditor()
    return editor.props.containerId
  }

  getEditor () {
    return this.context.editor
  }

  getSurface () {
    const { surfaceManager } = this.context
    const containerId = this.getContainerId()

    return surfaceManager.getSurface(containerId)
  }

  // TODO -- review how the side toolbar gets disabled
  isReadOnlyMode () {
    const surface = this.getSurface()
    if (!surface) return true         // HACK -- ???

    return surface.isReadOnlyMode()
  }
}

LeftSwitchTextTypeTool.command = 'switch-text-type'

export default LeftSwitchTextTypeTool
