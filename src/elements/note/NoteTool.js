import { AnnotationTool } from 'substance'

class NoteTool extends AnnotationTool {
  renderButton ($$) {
    const el = super.renderButton($$)

    const readOnly = this.isSurfaceReadOnly()
    if (readOnly === true) el.attr('disabled', 'true')

    return el
  }

  getSurface () {
    const surfaceManager = this.context.surfaceManager
    const containerId = this.context.editor.props.containerId

    return surfaceManager.getSurface(containerId)
  }

  isSurfaceReadOnly () {
    const surface = this.getSurface()
    if (!surface) return

    return surface.isReadOnlyMode()
  }
}

NoteTool.type = 'note'

export default NoteTool
