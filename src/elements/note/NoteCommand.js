import { InsertInlineNodeCommand } from 'substance'

class NoteCommand extends InsertInlineNodeCommand {
  createNodeData () {
    return {
      type: 'note'
    }
  }

  execute (params) {
    let editorSession = params.editorSession
    let nodeData = this.createNodeData()

    // TODO -- why is this necessary?
    editorSession.transaction((tx) => {
      return tx.insertInlineNode(nodeData)
    })
  }
}

NoteCommand.type = 'note'

export default NoteCommand
