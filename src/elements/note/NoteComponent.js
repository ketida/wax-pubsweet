/* eslint react/prop-types: 0 */
import { Component } from 'substance'

class NoteComponent extends Component {
  didMount () {
    // TODO -- this disables the toolbar
    // TODO -- unload listener on dispose
    // TODO -- this gets registered once for each note -- to fix use didUpdate
    // this should only call disableTools

    const provider = this.getProvider()
    const editorSession = this.getEditorSession()
    provider.on('active:changed', this.rerender, this)

    editorSession.onUpdate('', this.removeActiveIsolated, this)
  }

  removeActiveIsolated () {
    const provider = this.getProvider()
    const editorSession = this.getEditorSession()
    const selection = editorSession.getSelection()
    //
    if (!selection.isNull()) {
      this.el.removeClass('active')
      provider.emit('remove:activeIsolatedNote')
    }
  }

  render ($$) {
    const { node } = this.props

    const provider = this.getProvider()
    const active = provider.getActiveNote()

    const noteContent = this.props.node['note-content']

    const el = $$('span')
      .attr('note-content', noteContent)
      .addClass('sc-note')
      .on('click', this.onClick)

    if (node.id === active) el.addClass('active')

    return el
  }

  onClick () {
    const { node } = this.props
    const provider = this.getProvider()

    provider.setActiveNote(node.id)
  }

  getProvider () {
    return this.context.notesProvider
  }

  getEditorSession () {
    return this.context.editorSession
  }

  dispose () {
    this.props.node.off(this)
  }
}

export default NoteComponent
