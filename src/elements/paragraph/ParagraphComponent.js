import { TextBlockComponent } from 'substance'

class ParagraphComponent extends TextBlockComponent {
  didMount () {
    this.addClass('sc-paragraph')
  }
}

export default ParagraphComponent
