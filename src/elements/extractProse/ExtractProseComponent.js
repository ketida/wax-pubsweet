import { TextBlockComponent } from 'substance'

class ExtractProseComponent extends TextBlockComponent {
  render ($$) {
    const el = super.render($$)
    return el.addClass('sc-blockquote')
  }
}

export default ExtractProseComponent
