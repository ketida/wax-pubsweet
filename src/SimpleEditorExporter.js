import { keys, map } from 'lodash'

import { HTMLExporter } from 'substance'

// import SimpleArticle from './SimpleArticle'
// var ProseArticle = require('substance/packages/prose-editor/ProseArticle')
// var schema = ProseArticle.schema

class SimpleExporter extends HTMLExporter {
  constructor (config) {
    let converters = map(keys(config.converters.html), function (key) {
      return config.converters.html[key]
    })

    super({
      // schema: schema,
      converters: converters
      // DocumentClass: SimpleArticle
    })
  }

  convertDocument (doc, htmlEl) {
    // TODO delete second arg
    var elements = this.convertContainer(doc, this.state.containerId)
    var out = elements.map(function (el) {
      return el.outerHTML
    })
    return out.join('')
  }

  // TODO -- fix this so that this fn is not needed
  convertContainer (container) {
    if (!container) {
      throw new Error('Illegal arguments: container is mandatory.')
    }

    var doc = container.editorSession.getDocument()
    this.state.doc = doc
    var elements = []

    container.editorSession.document.data.nodes.body.nodes.forEach(function (id) {
      var node = doc.get(id)
      var nodeEl = this.convertNode(node)
      elements.push(nodeEl)
    }.bind(this))
    return elements
  }
}

export default SimpleExporter
