import { FontAwesomeIcon as Icon, TOC as Toc } from 'substance'

class TableOfContents extends Toc {
  constructor (props, args) {
    super(props, args)
    this.handleAction('tocEntrySelected', (nodeId) => {
      const editor = this.getEditor()
      editor.scrollTo(nodeId)

      const provider = this.getProvider()
      provider.activeEntry = nodeId

      this.rerender()
    })
    // this.on('toc:updated', this.reComputeEntries, this)
  }

  // TODO better way? after editor's initial render ,every change in the document is not
  // updated. Editor never emits the event "document:updated". Workourand for now On didMound execute
  // handleDocumentChange to decide if there is a change to be reflected in the TOC
  didMount () {
    this.context.editorSession.onUpdate('document', this.getTocEntries, this)
  }

  getTocEntries (change) {
    const tocProvider = this.getProvider()
    tocProvider.handleDocumentChange(change)
    this.rerender()
  }

  render ($$) {
    const { book, fragment } = this.props

    const tocProvider = this.getProvider()
    const activeEntry = tocProvider.activeEntry

    const latinLevel = {
      '1': 'I',
      '2': 'II',
      '3': 'III'
    }

    const tocEntries = $$('div')
      .addClass('se-toc-entries')
      .ref('tocEntries')

    const entries = tocProvider.getEntries()

    for (let i = 0; i < entries.length; i++) {
      const entry = entries[i]
      const level = entry.level

      const tocEntryEl = $$('a')
        .addClass('se-toc-entry')
        .addClass('sm-level-' + level)
        .attr({
          href: '#',
          'data-id': entry.id
        })
        .ref(entry.id)
        .on('click', this.handleClick)
        .append(
          $$(Icon, {icon: 'fa-caret-right'}),
          latinLevel[entry.level] + '.  ',
          entry.name
        )

      if (activeEntry === entry.id) {
        tocEntryEl.addClass('sm-active')
      }

      tocEntries.append(tocEntryEl)
    }

    const bookLink = $$('a')
      .attr({
        title: 'Back to book builder for ' + book.title
      })
      .append('Book: ' + book.title)
      .on('click', this.goToBookBuilder)

    const info = $$('div')
      .addClass('sc-toc-panel-info')
      .append(
        bookLink,
        $$('br'),
        'Chapter name: ' + fragment.title
      )

    const el = $$('div')
      .addClass('sc-toc-panel')
      .ref('panelEl')
      .append(
        info,
        tocEntries
      )

    const headerElement = $$('div')
      .addClass('sc-toc-panel-header')
      .append('Chapter Structure')

    const tocContent = $$('div')
      .addClass('sc-toc-wrapper')
      .append(
        headerElement,
        el
      )

    return $$('div')
      .addClass('sc-toc-container')
      .append(tocContent)
  }

  getEditor () {
    return this.context.editor
  }

  getProvider () {
    return this.context.tocProvider
  }

  goToBookBuilder () {
    const { book, history } = this.props
    const url = '/books/' + book.id + '/book-builder'

    history.push(url)
  }
}

export default TableOfContents
