import { clone, isEmpty, map, pickBy, sortBy, values } from 'lodash'
import { TOCProvider } from 'substance'

class NotesProvider extends TOCProvider {
  constructor (...props) {
    super(...props)
    this.activeNote = undefined
  }

  computeEntries () {
    const doc = this.getDocument()
    const nodes = doc.getNodes()

    // get all notes from the document
    const noteNodes = pickBy(nodes, (value, key) => {
      return value.type === 'note'
    })

    const sortedNoteEntries = this.sortNodes(noteNodes)

    return sortedNoteEntries
  }

  sortNodes (nodes) {
    const { containerId } = this.config

    let notes = clone(nodes)
    const doc = this.getDocument()
    const container = doc.get(containerId)

    // sort notes by
    //   the index of the containing block
    //   their position within that block

    notes = map(notes, function (note) {
      const blockId = note.path[0]
      const blockPosition = container.getPosition(blockId)
      const nodePosition = note.start.offset

      // TODO -- what is going on here?
      // we used to have 'content' instead of 'note-content'
      // and the node itself as a property of this object
      // they're gone
      return {
        id: note.id,
        'note-content': note['note-content'],
        blockPosition: blockPosition,
        nodePosition: nodePosition
      }
    })

    return sortBy(notes, ['blockPosition', 'nodePosition'])
  }

  // TODO -- refactor
  handleDocumentChange (change) {
    const noteCreated = values(change.created).filter(value => value.type === 'note')
    const noteDeleted = values(change.deleted).filter(value => value.type === 'note')
    const before = values(change.before).filter(value => value.type === 'null')

    if (!isEmpty(noteCreated) || !isEmpty(noteDeleted)) {
      this.emit('notes:updated')

      if (!isEmpty(noteCreated)) {
        const createdId = noteCreated[0].id
        this.setActiveNote(createdId)
      }
      return
    }

    if (isEmpty(before) && this.activeNote !== '') {
      this.activeNote = ''
      this.emit('notes:updated')
    }
  }

  getActiveNote () {
    return this.activeNote
  }

  setActiveNote (noteId) {
    this.activeNote = noteId
    this.emit('active:changed', noteId)
  }

  // calloutSelected (noteId) {
  //   this.emit('noteSelected', noteId)
  // }
}

NotesProvider.tocTypes = ['note']

export default NotesProvider
