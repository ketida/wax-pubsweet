/* eslint react/prop-types: 0 */

import _ from 'lodash'
import { Component } from 'substance'

import CommentBox from './CommentBox'

class CommentBoxList extends Component {
  constructor (props, args) {
    super(props, args)
    this.tops = []
  }

  didMount () {
    const provider = this.getProvider()
    provider.on('comments:updated', this.onCommentsUpdated, this)
    this.context.editorSession.onUpdate('document', this.getCommentEntries, this)

    this.setTops()
  }

  didUpdate () {
    this.setTops()
  }

  getCommentEntries (change) {
    const provider = this.getProvider()
    provider.handleDocumentChange(change)
    this.rerender()
  }

  render ($$) {
    const self = this
    const provider = self.getProvider()
    const entries = provider.getEntries()
    const activeEntry = provider.activeEntry
    const { comments, user } = self.props

    const listItems = entries.map(function (entry, i) {
      const active = (entry.id === activeEntry)
      return $$(CommentBox, {
        active: active,
        comments: comments[entry.id] || { data: [] },
        entry: entry,
        parent: self,
        user: user
      })
    })

    return $$('ul')
      .addClass('sc-comment-pane-list')
      .append(listItems)
  }

  update (id, newComments) {
    const { comments, fragment, update } = this.props

    if (!comments[id]) comments[id] = { data: [] }
    comments[id] = newComments

    fragment.comments = comments
    update(fragment)
  }

  setTops () {
    const provider = this.getProvider()
    const entries = provider.computeEntries()

    const activeEntry = provider.activeEntry

    this.calculateTops(entries, activeEntry)
  }

  // TODO -- why are we looking at css classes?
  // TODO move to helper section??
  hasCssClass (element, className) {
    return element.className && new RegExp('(^|\\s)' + className + '(\\s|$)').test(element.className)
  }

  calculateTops (entries, active) {
    let result = []
    let boxes = []
    let self = this
    _.each(entries, function (entry, pos) {
      // initialize annotationTop and boxHeight, as there is a chance that
      // annotation and box elements are not found by the selector
      // (as in when creating a new comment)
      let annotationTop = 0
      let boxHeight = 0
      let top = 0

      let isActive = false
      if (entry.id === active) isActive = true

      // get position of annotation in editor
      const annotationEl = document.querySelector('span[data-id="' + entry.id + '"]')

      if (annotationEl) {
        annotationTop = (self.hasCssClass(annotationEl.offsetParent, 'sc-isolated-node'))
        ? annotationEl.offsetParent.offsetTop + annotationEl.offsetTop : annotationEl.offsetTop
      }

      // get height of this comment box
      const boxEl = document.querySelector('li[data-comment="' + entry.id + '"]')
      if (boxEl) boxHeight = parseInt(boxEl.offsetHeight)

      // keep the elements to add the tops to at the end
      boxes.push(boxEl)

      // where the box should move to
      top = annotationTop

      // if the above comment box has already taken up the height, move down
      if (pos > 0) {
        let previousBox = entries[pos - 1]
        let previousEndHeight = previousBox.endHeight
        if (annotationTop < previousEndHeight) {
          top = previousEndHeight + 2
        }
      }

      // store where the box ends to be aware of overlaps in the next box
      entry.endHeight = top + boxHeight + 2
      result[pos] = top

      // if active, move as many boxes above as needed to bring it to the annotation's height
      if (isActive) {
        entry.endHeight = annotationTop + boxHeight + 2
        result[pos] = annotationTop

        let b = true
        let i = pos

        // first one active, none above
        if (i === 0) b = false

        while (b) {
          let boxAbove = entries[i - 1]
          let boxAboveEnds = boxAbove.endHeight
          let currentTop = result[i]

          let doesOverlap = boxAboveEnds > currentTop
          if (doesOverlap) {
            var overlap = boxAboveEnds - currentTop
            result[i - 1] -= overlap
          }

          if (!doesOverlap) b = false
          if (i <= 1) b = false
          i -= 1
        }
      }
    })

    // give each box the correct top
    _.each(boxes, function (box, i) {
      let val = result[i] + 'px'
      if (box) box.style.top = val
    })
  }

  getProvider () {
    return this.context.commentsProvider
  }

  onCommentsUpdated () {
    this.rerender()
  }

  dispose () {
    var provider = this.getProvider()
    provider.off(this)
  }
}

export default CommentBoxList
