/* eslint react/prop-types: 0 */

import { Component } from 'substance'

import Comment from './Comment'

class CommentList extends Component {
  render ($$) {
    const active = this.props.active
    const box = this.props.box
    const comments = this.props.comments

    var commentsEl = comments.map(function (comment) {
      return $$(Comment, {
        active: active,
        box: box,
        text: comment.text,
        user: comment.user
      })
    })

    return $$('div')
      .addClass('comment-list')
      .append(commentsEl)
  }
}

export default CommentList
